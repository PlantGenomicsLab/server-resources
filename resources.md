| Analysis | Software | Input(s) Size | Parameters | Memory Given | Cores Given | Memory Efficiency | CPU Efficiency | Job Completed Successfully | Recommended Resources | Notes | 
| --- | --- |  --- | ---  | --- | --- | --- | --- | --- | --- | --- |
| Build Database for Generating Repeat Library | RepeatModeler - Build DB | Genome Size = 25G | -engine ncbi | 400G | 48 | 0.84% = 3.36 GB | 2.03% | Yes | 2 cores + 5G |
| Generating Repeat Library | RepeatModeler | Genome Database = 6.5MB | -engine ncbi | 400G | 48 |  6.87% =  27.46 G |  19.50% | Yes | 10 cores + 30 G | 
| Soft Mask Genome | RepeatMasker | 53 M Genome + 1.9 M Repeat Libarary | -pa 10 -gff -a -noisy -xsmall | 10G | 10 | 89.00% = 8.9G | 96.67% | Yes | 10 cores + 10G | 
| Soft Mask Genome | RepeatMasker | 615 M Genome + 1.9 M Repeat Libarary | -pa 30 -gff -a -noisy -xsmall | 50G | 30 | 291.53% = 145G |  91.64% | Yes | 30 cores + 50G | Increase memory for faster run times
| Soft Mask Genome | RepeatMasker | 1.4 G Genome + 1.9 M Repeat Libarary | -pa 40 -gff -a -noisy -xsmall | 100G | 40 |  192.44% = 192G | 14.24% | Yes | 30 cores + 200G | 
| Index Genome | hisat2-build | 25G Genome | | 70G | 12 | 68.24% = 47.77 GB | 8.33% | Yes | 12 cores + 50 G |
| Align Short Reads to Genome | HISAT2 | 25G Genome + 42G library | -q --dta --max-intronlen 2000000 | 200G | 12 cores | 32.04% = 64.08G |  8.32% | Yes | 12 cores + 66G |  
| Index Genome | gmap_build | 25G Genome | | 100G | 16 | 42.85%  = 42.85 G | 6.28% | Yes | 12 cores + 44 G |


*Note general custom scripts written in python, R, perl, etc are NOT multi-threaded (unless you specifically made them so). Please use 1 core for executing these scripts.  
The memory used for these scripts should depend on the size of the input.  
*CPU Efficiency is a misleading metric as it also includes the time the job spends in the queue. 
