# Server Resources

This git repository is meant to document how much resources (cores and memory) is required for specific job to complete successfully.  
In order to document, we will be providing the analysis name, software used, size of input, parameters used, memory used, cores used,  
core efficieny, memory efficieny, and the recommended resources. This will not only make us good users (because we won’t be hogging  
all the resources) but it might help get our jobs in the running state faster. 
